"""Relatively simple functions that was making MainScript messy"""
HEADLESS = True
import numpy as np

if not HEADLESS:
    import matplotlib.pyplot as plt

def get_xy_pos(coords, z_pos, img):
    """Get xy-coordinates in the slice at z_pos"""
    x_cords = []
    y_cords = []
    for pos in coords:
        if (pos[0] - z_pos) < (0.5 * img.slice_distance):
            y_cords.append(int((pos[1] - img.yoffset) / img.pixel_size))
            x_cords.append(int((pos[2] - img.xoffset) / img.pixel_size))
    return (x_cords, y_cords)

def get_non_zero(bool_cube, voi):
    """Get values and coordinates of all non-zero indicies"""
    sum_bool = np.sum((bool_cube != 0).flatten())
    coords = np.zeros((sum_bool, 3))
    offsets = [voi.cube.zoffset,
               voi.cube.yoffset,
               voi.cube.xoffset]
    cit = 0
    for i in range(bool_cube.shape[0]):
        for j in range(bool_cube.shape[1]):
            for k in range(bool_cube.shape[2]):
                if bool_cube[i, j, k] != 0:
                    coords[cit, 0] = (i + 0.5) * voi.cube.slice_distance + offsets[0]
                    coords[cit, 1] = (j + 0.5) * voi.cube.pixel_size + offsets[1]
                    coords[cit, 2] = (k + 0.5) * voi.cube.pixel_size + offsets[2]
                    cit += 1

    return coords

def display_slice_with_voi(cube, voi, slice_pos):
    """Plot the slice at position, "slice_pos", with voi overlay"""
    if HEADLESS:
        return
    bool_cube = voi.get_voi_cube().cube

    tru_coords = get_non_zero(bool_cube, voi)

    x_coords, y_coords = get_xy_pos(tru_coords, slice_pos, cube)
    z_id = int((slice_pos - cube.zoffset) / cube.slice_distance)
    plt.figure(500)
    plt.imshow(cube.cube[z_id, :, :])
    plt.scatter(x_coords, y_coords, c="r")
    plt.show()

def get_center_point(cube):
    """Calculate and return the point in the center of cube (in mm)"""
    cube_size = np.array([cube.dimx, cube.dimy, cube.dimz])
    center_point = cube_size / 2.0
    return cube.indices_to_pos(center_point)
