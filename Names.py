"""A module for finding the volumes of interest based on naming scheme"""

class Names(object):
    """Class for finding VOI based on their naming scheme"""
    def __init__(self, vdx, patient, phase):
        """Initiates the Names Class and gets the VOI names"""
        self.names = vdx.get_voi_names()
        self.patient = patient
        self.phase = phase
        self.prostate_ptv, self.prostate_gtv, self.ptv_lap_rec = [""] * 3
        self.ptv_sub_gtv, self.rectum, self.bladder = [""] * 3

    def print_names(self):
        """Prints a list of the found names"""
        self.names.sort()
        print(self.names)

    def auto_prostate(self):
        """An automated script looping through all names catching those of
        interest"""
        success_dict = {"prostatePTV" : 0,
                        "prostateGTV" : 0,
                        "PTVrecLap" : 0,
                        "PTVsubGTV" : 0,
                        "bladder" : 0,
                        "rectum" : 0}
        self.names.sort()

        for inames in self.names:
            if "o ptv jacks" in inames.lower():
                self.prostate_ptv = inames
                success_dict["prostatePTV"] = 1
            elif "o gtv" in inames.lower():
                self.prostate_gtv = inames
                success_dict["prostateGTV"] = 1
            elif "o lap rec/ptvj" in inames.lower():
                self.ptv_lap_rec = inames
                success_dict["PTVrecLap"] = 1
            elif "o ptv sub gtv" in inames.lower():
                self.ptv_sub_gtv = inames
                success_dict["PTVrecLap"] = 1
            elif "rectum" in inames.lower() and not success_dict['rectum']:
                # air, post and wall are always in later iteration.
                self.rectum = inames
                success_dict["rectum"] = 1
            elif "vesica urinaria" in inames.lower():
                self.bladder = inames
                success_dict["bladder"] = 1
        print("Patient %s Phase %s found:"%(self.patient, self.phase))
        return success_dict
